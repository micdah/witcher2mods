class CItemDetail extends CObject
{
	var m_itemId				: SItemUniqueId;
	var m_itemIdx				: int;
	var m_itemName				: name;	
	var m_itemCategory			: name;
	var m_itemPrice				: int;
	var m_itemTags				: array<name>;
	var m_craftedItem			: CItemDetail;
	
	public final function LoadFromId(inventory : CInventoryComponent) : bool
	{
		var craftedItemName	: name;
		var craftedItem		: CItemDetail;
	
		if (this.m_itemId == GetInvalidUniqueId()) 
		{
			return false;
		}
		
		this.m_itemName		= inventory.GetItemName(this.m_itemId);
		this.m_itemCategory	= inventory.GetItemCategory(this.m_itemId);
		this.m_itemPrice	= theHud.m_utils.GetItemNamePrice(this.m_itemName, inventory);
		inventory.GetItemTags(this.m_itemId, this.m_itemTags);
		
		if (this.m_itemTags.Contains('Recipe') || this.m_itemTags.Contains('Schematic'))
		{
			craftedItemName = inventory.GetCraftedItemName(this.m_itemId);
			
			/*
			 * We have to add the item (briefly) to the inventory, in order to ensure that we can resolve
			 * a unique ID for it, and remove it immediately after we have obtained the needed information
			 */
			craftedItem = new CItemDetail in this;
			craftedItem.m_itemId = inventory.AddItem(craftedItemName, 1, false);
			craftedItem.LoadFromId(inventory);
			inventory.RemoveItem(craftedItem.m_itemId);
			
			this.m_craftedItem = craftedItem;
		}
		
		return true;
	}
	
	public final function LogState() 
	{
		Log(ToString());
	}
	
	public final function ToString() : string
	{
		var msg : string;
		
		msg = "Item details for #" + UniqueIdToString(this.m_itemId) + ": ";
		
		msg += "Name = '" + this.m_itemName + "'";
		msg += ", Category = '" + this.m_itemCategory + "'";
		msg += ", Tags = '" + ArrayToString(this.m_itemTags, ", ") + "'";
		
		if (this.m_craftedItem) 
		{
			msg += ", Crafted " + this.m_craftedItem.ToString();
		}
		
		return msg;
	}
}

/*
 * Loads item details for all item ID's currently in the m_mapItemIdxToId array 
 */
function LoadItemDetails(mapItemIdxToId : array<SItemUniqueId>, inventory : CInventoryComponent) : array<CItemDetail>
{
	var items		: array<CItemDetail>;
	var item		: CItemDetail;
	var itemId		: SItemUniqueId;
	var i, size		: int;
	
	size = mapItemIdxToId.Size();		
	for (i=0; i < size; i+=1) 
	{
		itemId			= mapItemIdxToId[i];
		item 			= new CItemDetail in inventory;
		item.m_itemId	= itemId;
		item.m_itemIdx	= i;	// Index in m_mapItemIdxToId
		if (item.LoadFromId(inventory))
		{
			items.PushBack(item);
		}
	}
	
	return items;
}

/*
 * Returns a list only containing the items that contain the specified tag (if any)
 */
function ListItemsContainingTag(items : array<CItemDetail>, tag : name) : array<CItemDetail>
{
	var result	: array<CItemDetail>;
	var item	: CItemDetail;
	var i, size	: int;
	
	// Iterate all items and isolate those containing the specified tag
	size = items.Size();
	for (i=0; i < size; i+=1)
	{
		item = items[i];
		if (item.m_itemTags.Contains(tag))
		{
			result.PushBack(item);
		}
	}
	
	return result;
}

function SortItemsByCraftedItem(out items : array<CItemDetail>)
{
	var i,j,size 	: int;
	var item		: CItemDetail;
	var strA, strB	: string;
	var cmp			: int;
				
	//Log("Sorting list:");
	//LogItems(items);
	
	size = items.Size();
	for (i=0; i < size; i+=1) 
	{
		//Logf("Processing index #%1", i);
		
		j = i;
		while (j > 0)
		{
			cmp = CompareItems(items[j-1].m_craftedItem, items[j].m_craftedItem);
			
			if (cmp > 0)
			{
				//Logf("Swapping element #%1 and #%2", j, j-1);
				item = items[j];
				items[j] = items[j-1];
				items[j-1] = item;
			} 
			else
			{
				// Skip items that are identical
				break;
			}
			
			j = j -1;
		}
	}
	
	//Log("Sorted list:");
	//LogItems(items);
}

function CompareItems(item1 : CItemDetail, item2 : CItemDetail) : int
{
	var strCmp			: int;
	
	// Handle if either or both are NULL
	if (!item1 && item2)
	{
		return -1;
	}
	if (item1 && !item2)
	{
		return 1;
	}
	if (!item1 && !item2)
	{
		return 0;
	}
			
	// If both are non-NULL, first compare category
	strCmp = StrCmp(item1.m_itemCategory, item2.m_itemCategory);
	if (strCmp != 0) 
	{
		return strCmp;
	}
	
	// Then compare based prices
	return item1.m_itemPrice - item2.m_itemPrice;
}

function LogItems(items : array<CItemDetail>) 
{
	var i,size	: int;
			
	size = items.Size();
	for (i=0; i < size; i+=1) 
	{
		items[i].LogState();
	}
}

function ShowItems(items : array<CItemDetail>)
{
	var msg		: string;
	var i, size	: int;
	var confirm : CGuiTextConfirm;
	
	size = items.Size();
	for (i=0; i < size; i+=1)
	{
		if (i == 0)
		{
			msg = items[i].m_craftedItem.m_itemName + " (" + items[i].m_craftedItem.m_itemCategory + ")";
		}
		else
		{
			msg += ", " + items[i].m_craftedItem.m_itemName + " (" + items[i].m_craftedItem.m_itemCategory + ")";
		}
	}
	
	confirm = new CGuiTextConfirm in theHud.m_messages;
	confirm.m_text = msg;
	theHud.m_messages.ShowConfirmationBox(confirm);
}

class CGuiTextConfirm extends CGuiConfirmationBox
{
	var m_text		: string;
	
	function GetText() 				: string	{ return m_text; }
	function GetSelectionOfEscape()	: bool		{ return false; }
	
	event OnYes() {}
	event OnNo() {}
}