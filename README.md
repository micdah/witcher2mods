# Witcher 2 Mods #

As many mods for Witcher 2 use the same `dzip` files, it is impossible to install mods that have _overlapping_ files. The fix for this is simply merging the mods - which is exactly what I have done.

## Mods included ##

* [Fix Dice Poker and Arm Wrestling - Always win](http://www.nexusmods.com/witcher2/mods/646/?)
    * by **Zeph Sunstrider**
    * _Both the arm wrestling and poker dice fix are included_
    * Included a patch to the original mod which fixes the draw-situation, such that the player always wins, regardless.
* [Extra talents per Lv](http://www.nexusmods.com/witcher2/mods/646/?)
    * by **kalpa2004**
    * _Just the basic version without any other mods, as I am merging myself_
* [Zero Weight Mod](http://www.nexusmods.com/witcher2/mods/265/?)
    * by **raven4444**
* [Win more Orens at Dice Poker](http://www.nexusmods.com/witcher2/mods/685/?)
    * by **Asrakorn**
    * _Only increased minimum stake from 10 (defualt) to 500 (as opposed to 2000 which is the value in this mod)_
	
## Changes unique to this mod ##

Aside from merging existing mods, I have also added my own changes in the merged mod, these changes are:

* **Remove enhancements**
    * Added the ability to remove enhancements from items that have no more free slots. This works by allowing you to choose to remove an enhancement when you try to add a new one, when there are no free slots.
        * Enhancements removed are lost forever, they are not added back to your inventory.
        * _In order for this to work, I have had to trig the GUI into believing there are always free slots on items, thus it will look like there is always 1 slot free, even on items with no free slots. This does not affect the description, in which you can always see the actual enhancements added._
    * Added the ability to remove enhancements from items when you try to install new enhancements
* **Sort recipes by category**
    * Recipes for crafting as well as for alchemy are now sorted by category, instead of being sorted simply by name. This means all similar recipes are together and not scattered throughout the list. Recipes within a category are further sorted by price, meaning the most expensive recipe within a category are to be found towards the bottom of the list.
* **Dice Poker**
    * Fix to mod _"Fix Dice Poker and Arm Wrestling - Always win"_: Player also wins in a draw situation (thus truely always winning)
    * Change to mod _"Win more Orens at Dice Poker"_: Changed minimum stake to 500 (default is 10, this is originally changed to 2000 using this mod)
    * Reduced wait times when playing
        * Such as lowering the "NPC Thinking" wait time, among other wait points
		
## Changelog ##

* Version 1.2 ([download](https://bitbucket.org/micdah/witcher2mods/src/13e3d61ab0c0be721d617b7922348608f34b5352/Deploy/Install.zip))
    * New feature: Recipes are sorted by category
        * Instead of having all your recipes sorted by name when crafting or doing alchemy, they are now sorted by category (and sub sorted by price), to make it easier to compare similar recipes.
        * In order to get the GUI to order properly, I have had to prefix all recipes with a number - it is not the best solution but alas the sorting is done in the SWF file which is outside the control of the scripts I alter.
* Version 1.1 ([download](https://bitbucket.org/micdah/witcher2mods/src/6544bdbf027b12ef13033d2ca00e3fd9916504df/Deploy/Install.zip))
    * New function: Remove enhancements
        * It is now possible to remove enhancements when an item has no more free slots
* Version 1.0 ([download](https://bitbucket.org/micdah/witcher2mods/src/0a9456f34c0463c6f2b549e9c85974201b59c1a1/Deploy/Install.zip))
	* This is the initial release which contains the mods: _"Fix Dice Poker and Arm Wrestling - Always win_", _"Extra talents per Lv"_, _"Zero Weight Mod"_ and _"Win more Orens at Dice Poker"_ as well as my own changes to Dice Poker including the change of speed to the game.
	
## Installation ##

1. Download the latest [Install.zip](https://bitbucket.org/micdah/witcher2mods/src/master/Deploy/Install.zip)
1. Locate your _Witcher 2_ installation folder
    * _If installed via Steam, you will find this folder at `SteamApps\common\the witcher 2\`_
1. Make a backup of the file `CookedPC\base_scripts.dzip` 
    * (_for example by simply renaming the file by adding a `.bak` extension to it_)
1. Unpack the file `Install.zip` into `CookedPC\`
1. **All done**

## Uninstallation ##

1. Delete file `CookedPC\base-scripts.dzip` and restore your backup version of this file
	* (_if you simply renamed it, you can remove the `.bak` to restore_)
	* (_alternatively you can fetch a vanilla version from `Vanilla files\base_scripts.dzip`_)
1. Remove the folder `CookedPC\items`
1. **All done**