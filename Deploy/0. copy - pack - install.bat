@ECHO OFF
SET          Debug=False
SET     MergedPath=..\Merged mods
SET     REDkitPath=C:\Program Files (x86)\The Witcher 2 REDkit

SET DebugCheck=IF "%Debug%"=="True" PAUSE
GOTO COPY


:COPY
CALL 1.copy
%DebugCheck%


:PACK
CALL 2.pack
%DebugCheck%


:INSTALL
CALL 3.install
%DebugCheck%