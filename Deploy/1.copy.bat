@ECHO OFF
SET          Debug=False
SET     MergedPath=..\Merged mods
SET     REDkitPath=D:\Games\The Witcher 2 Enhanced Edition (GOG)

ECHO Copying scripts from REDkit to Merged path
SET DebugCheck=IF "%Debug%"=="True" PAUSE

GOTO COPY


:COPY
XCOPY /E /S /R /Y /Q "%REDkitPath%\bin\scripts" "%MergedPath%\base_scripts\"
%DebugCheck%