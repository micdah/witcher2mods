@ECHO OFF
SET          Debug=False
SET     DeployPath=.\Install.zip
SET      BuildPath=.\build
SET     MergedPath=..\Merged mods
SET      ToolsPath=..\Tools
SET   RedToolsPath=%ToolsPath%\Gibbed's RED Tools
SET        RedPack=%RedToolsPath%\Gibbed.RED.Pack.exe
SET      RedUnpack=%RedToolsPath%\Gibbed.RED.Unpack.exe
SET     SevenzPath=%ToolsPath%\7za920
SET        SevenZa=%SevenzPath%\7za.exe


ECHO Creating Install archive
SET DebugCheck=IF "%Debug%"=="True" PAUSE

GOTO CLEAN_BUILD_FOLDER


:CLEAN_BUILD_FOLDER
IF NOT EXIST %BuildPath% GOTO CREATE_BUILD_FOLDER
ECHO ^> Deleting previous builder folder "%BuildPath%"
RMDIR /S /Q %BuildPath%
%DebugCheck%


:CREATE_BUILD_FOLDER
ECHO ^> Creating build folder "%BuildPath%"
MKDIR %BuildPath%
%DebugCheck%


:PACK_BASE_SCRIPTS
ECHO ^> Packing base_scripts
"%RedPack%" "%BuildPath%\base_scripts.dzip" "%MergedPath%\base_scripts"
%DebugCheck%


:COPY_ITEMS_FOLDER
ECHO ^> Copying items folder
XCOPY /E /S /Q "%MergedPath%\items" "%BuildPath%\items\" >nul 2>&1
%DebugCheck%


:CLEAN
IF NOT EXIST %DeployPath% GOTO PACK
DEL /Q %DeployPath%
%DebugCheck%


:PACK
ECHO ^> Packing into install archive
"%SevenZa%" a -tzip "%DeployPath%" "%BuildPath%\*" >nul 2>&1
%DebugCheck%