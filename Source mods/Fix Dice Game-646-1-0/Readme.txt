Fixed Dice Poker game - ReadMe

This mod changes an equation in the game's script handling during dice poker, resulting in the player always winning, no matter what dice hands were thrown.

Just extract the .dzip file into the game's CookedPC folder, overwrite the original one AFTER making a backup.

Based on the game's own base_scripts.dzip - Vanilla.
If you have script mods in that archive, add the file game\minigames\dicepoker.ws to your archive using gibbed's REDPack program.

I hope this makes the dice poker a little less desk-chewingly frustrating.

Cheers, and enjoy!

- Zeph