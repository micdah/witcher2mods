=====================================================================
== Dynamic HUD v1.0
== tested with: TW2 Enhanced Edition (GOG, eng, initial release)
== by Presskohle
== Contact: Presskohle @ http://en.thewitcher.com/forum/
=====================================================================

=== DESCRIPTION
- reduced default HUD Elements
- useful HUD Elements fade in & out when engaged in Combat and outside
- Outside of Combat, open the Quick Menu to check on hidden HUD Elements

Check the provided screenshots for more info.

=== INSTALL
Just copy "dynamichud.dzip" into your "\TW2 installation folder\CookedPC\" folder.

=== UNINSTALL
Delete "dynamichud.dzip" from your "\TW2 installation folder\CookedPC\" folder.