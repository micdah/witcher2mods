Fixed Dice Poker and Arm Wrestling - ReadMe

This mod changes an equation in the game's script handling during dice poker and armwrestling, resulting in the player always winning, no matter what dice hands were thrown or how the wrestle went.
Don't worry about losing a wrestle even if the bar heads for the wrong side - you'll still win, and due to the equation change it has the added comical effect of Geralt slamming his opponent's wrist 180 degrees to the other side.
This way he even visually wins in the end. He IS rather strong after all.

Just extract the .dzip file into the game's CookedPC folder, overwrite the original one AFTER making a backup.

Based on the game's own base_scripts.dzip - Vanilla.
If you have script mods in that archive, add the file game\minigames\dicepoker.ws to your archive using gibbed's REDPack program.

I hope this makes the dice poker a little less desk-chewingly frustrating.

Cheers, and enjoy!

- Zeph